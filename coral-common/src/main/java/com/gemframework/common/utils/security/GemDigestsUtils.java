/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.common.utils.security;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.SecureRandom;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

@Slf4j
public class GemDigestsUtils {
    private static final String SHA1 = "SHA-1";
    private static final String SHA256 = "SHA-256";
    private static final String MD5 = "MD5";
    private static SecureRandom random = new SecureRandom();

    public GemDigestsUtils() {
    }

    //==========================字节加密==========================

    //通过SHA1算法加密
    public static byte[] sha1(byte[] input) {
        return digest(input, SHA1, (byte[])null, 1);
    }

    public static byte[] sha1(byte[] input, byte[] salt) {
        return digest(input, SHA1, salt, 1);
    }

    public static byte[] sha1(byte[] input, byte[] salt, int iterations) {
        return digest(input, SHA1, salt, iterations);
    }

    //通过SHA256算法加密
    public static byte[] sha256(byte[] input) {
        return digest(input, SHA256, (byte[])null, 1);
    }

    public static byte[] sha256(byte[] input, byte[] salt) {
        return digest(input, SHA256, salt, 1);
    }

    public static byte[] sha256(byte[] input, byte[] salt, int iterations) {
        return digest(input, SHA256, salt, iterations);
    }


    //通过MD5算法加密
    public static byte[] md5(byte[] input) {
        return digest(input, MD5, null, 1);
    }

    private static byte[] digest(byte[] input, String algorithm, byte[] salt, int iterations) {
        try {
            MessageDigest digest = MessageDigest.getInstance(algorithm);
            if (salt != null) {
                digest.update(salt);
            }

            byte[] result = digest.digest(input);

            for(int i = 1; i < iterations; ++i) {
                digest.reset();
                result = digest.digest(result);
            }

            return result;
        } catch (GeneralSecurityException var7) {
            log.error(var7.getMessage());
            return null;
        }
    }

    /***
     * 生成盐
     * @param numBytes 位数
     * @return
     */
    public static byte[] generateSalt(int numBytes) {
        Validate.isTrue(numBytes > 0, "numBytes argument must be a positive integer (1 or larger)", numBytes);
        byte[] bytes = new byte[numBytes];
        random.nextBytes(bytes);
        return bytes;
    }



    //==========================流加密==========================
    /**
     * MD5加密
     * @param input
     * @return
     * @throws IOException
     */
    public static byte[] md5(InputStream input) throws IOException {
        return digest(input, MD5);
    }

    public static byte[] sha1(InputStream input) throws IOException {
        return digest(input, SHA1);
    }

    public static byte[] sha256(InputStream input) throws IOException {
        return digest(input, SHA256);
    }

    private static byte[] digest(InputStream input, String algorithm) throws IOException {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
            int bufferLength = 8192;
            byte[] buffer = new byte[bufferLength];

            for(int read = input.read(buffer, 0, bufferLength); read > -1; read = input.read(buffer, 0, bufferLength)) {
                messageDigest.update(buffer, 0, read);
            }

            return messageDigest.digest();
        } catch (GeneralSecurityException var6) {
            log.error(var6.getMessage());
            return null;
        }
    }
}
