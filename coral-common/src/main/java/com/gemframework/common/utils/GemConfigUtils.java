/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @Title: GemConfigUtils
 * @Package: com.gemframework.common.utils
 * @Date: 2020-07-30 22:14:37
 * @Version: v1.0
 * @Description: 配置文件工具类
 * @Author: nine QQ 769990999
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Slf4j
@Component
public class GemConfigUtils {

    /**
     * 读取yml配置文件
     * @param key
     * @return
     */
    public static Object getYmlConfig(String key){
        Resource resource = new ClassPathResource("/application.yml");
        Properties properties;
        try {
            YamlPropertiesFactoryBean yamlFactory = new YamlPropertiesFactoryBean();
            yamlFactory.setResources(resource);
            properties =  yamlFactory.getObject();
        } catch (Exception e) {
            log.info("配置文件读取失败："+e.getMessage());
            return null;
        }
        return properties.get(key);
    }

    /**
     * 读取Properties配置文件
     * @param key
     * @return
     */
    public static Object getProperties(String key){
        Properties prop = new Properties();
        InputStream in = Object.class.getResourceAsStream("classpath:/application.properties");
        try {
            prop.load(in);
        } catch (Exception e) {
            log.info("配置文件读取失败："+e.getMessage());
            return null;
        }
        return prop.getProperty(key).trim();
    }
}