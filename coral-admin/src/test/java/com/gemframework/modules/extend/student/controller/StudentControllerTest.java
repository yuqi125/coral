package com.gemframework.modules.extend.student.controller;

import com.gemframework.GemAdminApplication;
import com.gemframework.modules.extend.student.entity.Student;
import com.gemframework.modules.extend.student.service.StudentService;
import com.gemframework.service.impl.DatasourceServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GemAdminApplication.class)
@AutoConfigureMockMvc

@Slf4j
class StudentControllerTest {

    @Resource
    private StudentService studentService;

    @Autowired
    private DatasourceServiceImpl dbChangeServiceImpl;


    @Test
    void save() {

        Student student = new Student();
        student.setName("测试1");
        studentService.save(student);
        log.info("保存成功！");
    }

    @Test
    void find() {
        //切换到数据库dbtest2
        String dsName = "dbtest3";
        dbChangeServiceImpl.change(dsName);
        List<Student> userList = studentService.list();
        System.out.println(userList.toString());
    }
}