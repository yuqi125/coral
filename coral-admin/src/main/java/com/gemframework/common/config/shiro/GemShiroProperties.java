/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.common.config.shiro;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("gem.shiro")
public class GemShiroProperties {

	//cookie名称 = "rememberMe";
	private String remembermeCookieName;
	//记住我时长 = 604800;
	private int remembermeCookieMaxAge;
	//cookie加密key，防止服务器重启报错  = "Qtr5KRwI4qBJPrhH";
	// org.apache.shiro.crypto.CryptoException: Unable to execute 'doFinal' with cipher instance
	private String remembermeCipherkey;


}
