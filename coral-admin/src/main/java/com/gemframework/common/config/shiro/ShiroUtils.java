/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.common.config.shiro;
import com.gemframework.model.entity.po.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.SimpleHash;

import static com.gemframework.common.constant.GemSessionKeys.CURRENT_USER_KEY;

/**
 * 加密工具栏
 */
public class ShiroUtils {

	//加密算法名称
	public final static String HASH_ALGORITHM_NAME = "SHA-256";
	//加密次数
	public final static int HASH_ITERATIONS = 5;

	/***
	 * 密码加密（单向，不可逆）
	 * @param password
	 * @param salt 加盐
	 * @return
	 */
	public static String passwordSHA256(String password, String salt) {
		return new SimpleHash(HASH_ALGORITHM_NAME, password, salt, HASH_ITERATIONS).toString();
	}

	/***
	 * 获取用户
	 * @return
	 */
	public static User getUser() {
		return (User) SecurityUtils.getSubject().getSession().getAttribute(CURRENT_USER_KEY);
	}

}
