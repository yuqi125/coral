/**
 * 严肃声明：
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.modules.prekit.quartz.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gemframework.model.common.BaseEntityVo;
import com.gemframework.model.common.validator.SuperValidator;
import com.gemframework.modules.prekit.quartz.entity.validator.JobTypeCronValidator;
import com.gemframework.modules.prekit.quartz.entity.validator.JobTypeSimpleValidator;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Data
public class JobsVo extends BaseEntityVo {

    //任务名称
    @NotBlank(message = "任务名称不能为空",groups = SuperValidator.class)
    private String jobName;
    //任务组名称
    @NotBlank(message = "任务组名称式不能为空",groups = SuperValidator.class)
    private String jobGroup;
    //触发器名称
    @NotBlank(message = "触发器名称不能为空",groups = SuperValidator.class)
    private String triggerName;
    //触发器组名称
    @NotBlank(message = "触发器组名称不能为空",groups = SuperValidator.class)
    private String triggerGroup;

    //任务类型 Simple 和 Cron
    @NotNull(message = "任务类型不能为空",groups = SuperValidator.class)
    private Integer jobType;
    //任务参数
    private String jobParams;

    //Cron任务表达式
    @NotBlank(message = "Cron表达式不能为空",groups = JobTypeCronValidator.class)
    private String triggerCronExpression;

    //执行开始时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @NotNull(message = "执行时间不能为空",groups = JobTypeSimpleValidator.class)
    private Date triggerStartTime;

    //执行结束时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date triggerStopTime;

    //SpringBean类名称
    @NotBlank(message = "执行类名称不能为空")
    private String executeClassName;
    //SpringBean方法名称
    private String executeMethodName;
    //任务表名
    private String targetTable;
    //运行状态
    private Integer status;

}
