/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.modules.prekit.sys;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gemframework.common.annotation.SysLog;
import com.gemframework.common.constant.GemModules;
import com.gemframework.common.utils.GemBeanUtils;
import com.gemframework.controller.BaseController;
import com.gemframework.model.common.BaseResultData;
import com.gemframework.model.common.PageInfo;
import com.gemframework.model.common.validator.SaveValidator;
import com.gemframework.model.common.validator.TestDatasourceValidator;
import com.gemframework.model.common.validator.UpdateValidator;
import com.gemframework.model.entity.po.Datasource;
import com.gemframework.model.entity.vo.DatasourceVo;
import com.gemframework.model.enums.ExceptionCode;
import com.gemframework.model.enums.OperateType;
import com.gemframework.service.DatasourceService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Title: DatasourceController
 * @Date: 2020-07-01 17:58:04
 * @Version: v1.0
 * @Description: 动态数据源表控制器
 * @Author: gem
 * @Email: gemframe@163.com
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Slf4j
@RestController
@RequestMapping(GemModules.PreKit.PATH_PRE+"/sys/datasource")
public class DatasourceController extends BaseController {

    private static final String moduleName = "动态数据源表";

    @Autowired
    private DatasourceService datasourceService;

    /**
     * 获取列表分页
     * @return
     */
    @GetMapping("/page")
    @RequiresPermissions("datasource:page")
    public BaseResultData page(PageInfo pageInfo, DatasourceVo vo) {
        QueryWrapper queryWrapper = makeQueryMaps(vo);
        Page page = datasourceService.page(setOrderPage(pageInfo),queryWrapper);
        return BaseResultData.SUCCESS(page.getRecords(),page.getTotal());
    }
    /**
     * 获取列表
     * @return
     */
    @GetMapping("/list")
    @RequiresPermissions("datasource:list")
    public BaseResultData list(DatasourceVo vo) {
        QueryWrapper queryWrapper = makeQueryMaps(vo);
        List list = datasourceService.list(queryWrapper);
        return BaseResultData.SUCCESS(list);
    }

    /**
     * 添加
     * @return
     */
    @SysLog(type = OperateType.ALTER,value = "保存"+moduleName)
    @PostMapping("/save")
    @RequiresPermissions("datasource:save")
    public BaseResultData save(@RequestBody DatasourceVo vo) {
        GemValidate(vo, SaveValidator.class);
        Datasource entity = GemBeanUtils.copyProperties(vo, Datasource.class);
        datasourceService.save(entity);
        return BaseResultData.SUCCESS(entity);
    }


    /**
     * 删除 & 批量刪除
     * @return
     */
    @SysLog(type = OperateType.ALTER,value = "删除"+moduleName)
    @PostMapping("/delete")
    @RequiresPermissions("datasource:delete")
    public BaseResultData delete(Long id, String ids) {
        datasourceService.delete(id,ids);
        return BaseResultData.SUCCESS();
    }


    /**
     * 编辑
     * @return
     */
    @SysLog(type = OperateType.ALTER,value = "编辑"+moduleName)
    @PostMapping("/update")
    @RequiresPermissions("datasource:update")
    public BaseResultData update(@RequestBody DatasourceVo vo) {
        GemValidate(vo, UpdateValidator.class);
        Datasource entity = GemBeanUtils.copyProperties(vo, Datasource.class);
        datasourceService.update(entity);
        return BaseResultData.SUCCESS(entity);
    }


    /**
     * 获取用户信息ById
     * @return
     */
    @SysLog(type = OperateType.NORMAL,value = "查看"+moduleName)
    @GetMapping("/info")
    @RequiresPermissions("datasource:info")
    public BaseResultData info(Long id) {
        Datasource info = datasourceService.getById(id);
        return BaseResultData.SUCCESS(info);
    }

    /**
     * 测试连接
     * @return
     */
    @SysLog(type = OperateType.NORMAL,value = "测试连接"+moduleName)
    @PostMapping("/test")
    @RequiresPermissions("datasource:info")
    public BaseResultData test(DatasourceVo vo) {
        GemValidate(vo, TestDatasourceValidator.class);
        Datasource entity = GemBeanUtils.copyProperties(vo, Datasource.class);
        boolean conn = datasourceService.testConnection(entity);
        if(!conn){
            return BaseResultData.ERROR(ExceptionCode.DATASOURCE_CONNECT_FAIL);
        }
        return BaseResultData.SUCCESS();
    }

}