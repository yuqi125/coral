/**
 * 严肃声明：
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.modules.prekit.quartz.entity.enums;

import lombok.Getter;

/**
 * @Title: JobStatus.java
 * @Package: com.gemframework.enum
 * @Date: 2019/11/27 22:28
 * @Version: v1.0
 * @Description: 枚举类

 * @Author: zhangysh
 * @Copyright: Copyright (c) 2019 GemStudio
 * @Company: www.gemframework.com
 */
@Getter
public enum JobInit {
    //定时任务初始化方式：
    // 1:初始化全部停止状态；
    // 2:初始化全部运行状态，并启动任务
    // 3:保持原状态，并启动任务
    ALL_STOP_STATUS(1,"初始化全部停止状态"),
    ALL_START_STATUS(2,"初始化全部运行状态，并启动任务"),
    KEEP_START_STATUS(3,"保持原状态，并启动任务"),
    ;


    private Integer code;
    private String msg;

    JobInit(Integer code, String msg){
        this.code = code;
        this.msg = msg;
    }
}
