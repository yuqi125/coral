//获取微信
function getWechat() {
    layer.photos({
        photos: {
            "data": [{
                "src": "coral/images/769990999.png",
            }]
        }
        ,area: ['320px','435px']
        ,anim: 0
        ,end: function(){
            layer.closeAll();
        }
        ,success: function(layero){
            layer.tips('您可以添加开发者咨询', layero, {
                tips: [1, '#3595CC']
                ,time: 0
            });
        }
    });
}

function helloGem() {
    /*layer弹出一个示例*/
    layer.open({
        type: 1
        ,skin: 'layui-layer-admin'
        ,title: false //不显示标题栏
        ,closeBtn: false
        ,area: '300px;'
        ,shade: 0.8
        ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
        ,btn: ['逛逛社区','理解万岁']
        ,btnAlign: 'c'
        ,moveType: 1 //拖拽模式，0或者1
        ,content: '    <div style="padding: 50px; line-height: 22px; background-color: #1e9fff; color: #fff; font-weight: 300;">\n' +
            '        Hi，有缘人<br>\n' +
            '        初次见面便与你一见如故<br>\n' +
            '        你是我今生最美丽的相遇...<br>\n' +
            '        <hr>\n' +
            '        <legend>温馨提示：</legend>\n' +
            '        由于服务器资源有限，演示系统给您带来不便的体验<br>\n' +
            '        敬请谅解~~~~~\n' +
            '    </div>'
        ,success: function(layero){
            var btn = layero.find('.layui-layer-btn');
            btn.find('.layui-layer-btn0').attr({
                href: 'http://www.gemframework.com/bbs'
                ,target: '_blank'
            });
        }
    });
}

//获取shiro工具栏按钮
function shiroToolbar(shiroSave,shiroDelete,saveBtnName,delBtnName) {

    //工具栏 按钮
    var toolbarHtml ='<p>';
    if(shiroSave){
        toolbarHtml += '<button lay-event="add" class="layui-btn layui-btn-sm icon-btn">' +
            '<i class="layui-icon">&#xe654;</i>'+(typeof(saveBtnName) == "undefined"?"添加":saveBtnName)+'</button>&nbsp;';
    }
    if(shiroDelete){
        toolbarHtml += '<button lay-event="del" class="layui-btn layui-btn-sm layui-btn-danger icon-btn">' +
            '<i class="layui-icon">&#xe640;</i>'+(typeof(delBtnName) == "undefined"?"删除":delBtnName)+'</button>';
    }
    toolbarHtml += '</p>';
    return toolbarHtml;
}

//获取shiro右键绑定按钮
function shiroBindCtxMenu(shiroDelete,shiroUpdate) {
    //右键绑定按钮
    var bindCtxMenu = [];
    if(shiroDelete){
        bindCtxMenu.push({
            icon: 'layui-icon layui-icon-close text-danger',
            name: '<span class="text-danger">删除</span>',
            click: function (d) {
                doDel(d);
            }
        })
    }
    if(shiroUpdate){
        bindCtxMenu.push({
            icon: 'layui-icon layui-icon-edit',
            name: '修改',
            click: function (d) {
                showEditModel(d);
            }
        })
    }
    return bindCtxMenu;
}

function previewImg(src) {
    var imgHtml = "<img src='" + src + "' />";
    //捕获页
    var setting = {
        type: 1,
            shade: 0.8,
        area: ['100%', '100%'],
        shadeClose: true,
        closeBtn: false,
        offset: '100px',
        title: false, //不显示标题
        content: imgHtml, //捕获的元素，注意：最好该指定的元素要存放在body最外层，否则可能被其它的相对元素所影响
        cancel: function () {
        //layer.msg('捕获就是从页面已经存在的元素上，包裹layer的结构', { time: 5000, icon: 6 });
        }
    }

    var windowH = $(window).height();
    var windowW = $(window).width();

    getImageWidth(src,function(w,h){
        //console.log("win:"+windowH+","+windowW);
        //console.log("img:"+h+","+w);
        // 调整图片大小
        if(w>windowW || h>windowH){
            if(w>windowW && h>windowH){
                w = thisimg .width()*3;
                h = thisimg .height()*3;
                setting.area = [w+"px",h+"px"];
            }else if(w>windowW){
                setting.area = [windowW+"px",windowW*0.5/w*h+"px"];
            }else{
                setting.area = [w+"px",windowH+"px"];
            }
        }else{
            setting.area = [w+"px",h+"px"];
        }
        // 设置layer
        layer.open(setting);
    });
}

function getImageWidth(url,callback){
    var img = new Image();
    img.src = url;

    // 如果图片被缓存，则直接返回缓存数据
    if(img.complete){
        callback(img.width, img.height);
    }else{
        // 完全加载完毕的事件
        img.onload = function(){
            callback(img.width, img.height);
        }
    }
}

function runStr(digit){
    if(digit=="" || isNaN(digit)){
        alert("请输入数字");
    }else{
        var sourceStr="0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G";
        arrStr=sourceStr.split(",");
        var result=""; //定义变量并初始化
        var index=0;
        for(i=0;i<digit;i++){
            index=parseInt(Math.random()*arrStr.length);
            result+=arrStr[index];
        }
        return result;
    }
}

/**
 * 特殊字符转义 防止XSS攻击 用于特殊字符正常显示
 * @param text
 * @constructor
 */
function StringFilter(str) {
    var s = "";
    if (str.length === 0) {
        return "";
    }
    s = str.replace(/&/g, "&amp;");
    s = s.replace(/</g, "&lt;");
    s = s.replace(/>/g, "&gt;");
    s = s.replace(/ /g, "&nbsp;");
    s = s.replace(/\'/g, "&#39;");
    s = s.replace(/\"/g, "&quot;");
    return s;
}

/**
 *  转义字符还原成html字符
 * @param str
 * @returns {string}
 * @constructor
 */
function StringValFilter(str) {
    var s = "";
    if (str.length === 0) {
        return "";
    }
    s = str.replace(/&amp;/g, "&");
    s = s.replace(/&lt;/g, "<");
    s = s.replace(/&gt;/g, ">");
    s = s.replace(/&nbsp;/g, " ");
    s = s.replace(/&#39;/g, "\'");
    s = s.replace(/&quot;/g, "\"");
    return s;
}
