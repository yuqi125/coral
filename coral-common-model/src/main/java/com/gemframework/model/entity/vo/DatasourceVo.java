/**
 * 严肃声明：
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.model.entity.vo;

import com.gemframework.model.common.BaseEntityVo;
import com.gemframework.model.common.validator.SaveValidator;
import com.gemframework.model.common.validator.SuperValidator;
import com.gemframework.model.common.validator.TestDatasourceValidator;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Title: DatasourceVo
 * @Package: com.gemframework.model.entity.po
 * @Date: 2020-06-29 20:56:48
 * @Version: v1.0
 * @Description: 数据源信息
 * @Author: nine QQ 769990999
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Data
public class DatasourceVo extends BaseEntityVo {

    @NotBlank(message = "数据源名称不能为空",groups = {SaveValidator.class})
    String name;
    @NotBlank(message = "数据源URL不能为空",groups = {SaveValidator.class, TestDatasourceValidator.class})
    String url;
    @NotBlank(message = "数据源用户名不能为空",groups = {SaveValidator.class, TestDatasourceValidator.class})
    String username;
    @NotBlank(message = "数据源密码不能为空",groups = {SaveValidator.class, TestDatasourceValidator.class})
    String password;
    @NotBlank(message = "数据源驱动不能为空",groups = {SaveValidator.class, TestDatasourceValidator.class})
    String driverClassName;
    Integer connStatus;
}