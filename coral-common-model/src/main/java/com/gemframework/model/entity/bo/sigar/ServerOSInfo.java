/**
 * 严肃声明：
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.model.entity.bo.sigar;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gemframework.model.enums.OssStorageType;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
@Builder
public class ServerOSInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	//操作系统（内核）:    x64
	private String arch;
	//操作系统CpuEndian():    little
	private String cpuEndian;
	//操作系统DataModel():    64
	private String dataModel;
	//操作系统的描述:    Microsoft Windows 7
	private String description;
	//操作系统的卖主:    Microsoft
	private String vendor;
	//操作系统的卖主名:    Vienna
	private String vendorCodeName;
	//操作系统名称:    Windows 7
	private String vendorName;
	//操作系统卖主类型:    7
	private String vendorVersion;
	//操作系统的版本号:    6.2
	private String version;
}
