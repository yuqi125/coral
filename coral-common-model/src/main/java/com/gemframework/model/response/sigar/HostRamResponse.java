/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.model.response.sigar;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Slf4j
@Data
@Builder
public class HostRamResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    //总内存	7.64GB	1909.5MB
    private double hostMemory;
    //已用内存	5.24GB	1434.4MB
    private double hostMemoryUsed;
    //剩余内存	2.4GB	475.1MB
    private double hostMemoryFree;
    //使用率
    private double hostUsePercent;
    //swap总内存	7.64GB	1909.5MB
    private double swapMemory;
    //swap已用内存	5.24GB	1434.4MB
    private double swapMemoryUsed;
    //swap剩余内存	2.4GB	475.1MB
    private double swapMemoryFree;
    //使用率
    private double swapUsePercent;

}
