/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.model.common;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Title: RefreshTokenResponse
 * @Package: com.gemframework.modules.prekit.auth.entity.response
 * @Date: 2020-06-27 11:00:14
 * @Version: v1.0
 * @Description: 刷新AccessToken响应对象
 * @Author: nine QQ 769990999
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Data
public class ApiToken implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long memberId;
	private String accessToken;
	private String refreshToken;
	private Date accessTokenExpireTime;
	private Date refreshTokenExpireTime;
	private Date updateTime;

}
