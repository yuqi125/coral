## :tw-1f1ec:  :tw-1f1ea:  :tw-1f1f2:  :tw-1f1eb:  :tw-1f1f7:  :tw-1f1e6:   :tw-1f1f2:  :tw-1f1ea:     
## **支持我就帮忙在上方依次Watch、Star一波再离开，感激支持！**  
:tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345: :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345: :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345: :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345: :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345: :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345: :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345: :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345: :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345: :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345:  :tw-1f345: 
***
![输入图片说明](https://images.gitee.com/uploads/images/2020/0405/203759_f05dec3c_1388237.png "gemlogo.png")
***
[![License](https://img.shields.io/badge/License-MIT-blue)](http://www.gemframework.com) [![SpringBoot](https://img.shields.io/badge/SpringBoot-v2.2.x-blue)](https://spring.io/projects/spring-boot/)  [![MyBatis-Plus](https://img.shields.io/badge/MyBatisPlus-v3.3.x-yellow)](https://spring.io/projects/spring-boot/)  [![Shiro](https://img.shields.io/badge/Shiro-v1.4.x-brightgreen)](http://shiro.apache.org/) [![Layui](https://img.shields.io/badge/Layui-v2.5.6-orange)](http://www.layui.com/) [![Coral](https://img.shields.io/badge/Coral-v1.0.0-blue)](http://www.gemframework.com/) [![QQ1群](https://img.shields.io/badge/QQ%E7%BE%A4:72940788-%E6%BB%A1-red)](https://shang.qq.com/wpa/qunwpa?idkey=c39908fa28cb73b3e85d697436a52ca91e66b8870d020fcf4f555c51dca13b9a) [![QQ2群](https://img.shields.io/badge/QQ%E2%91%A1%E7%BE%A4-650255887-green)](https://shang.qq.com/wpa/qunwpa?idkey=c39908fa28cb73b3e85d697436a52ca91e66b8870d020fcf4f555c51dca13b9a)

## Release-v1.1.10 (2020.7.5) 【SaaS多租户版本版本 企业永久专享】[演示站点](http://coral.gemframework.com:8089/admin/login_gitee.html)
![输入图片说明](https://images.gitee.com/uploads/images/2020/0710/102145_4c21109c_2671379.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0710/102154_8b6e950b_2671379.png "2.png")
<!--![输入图片说明](https://images.gitee.com/uploads/images/2020/0706/230415_4f3f9353_2671379.png "多租户-1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0706/230431_75b86af7_2671379.png "多租户-2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0706/230443_ad3f11ca_2671379.png "多租户-3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0706/230450_930b67b4_2671379.png "多租户-4.png") -->

#### Beta-v1.0.10 (2020.7.5)
1. 修复阿里云OSS上传文件判断文件无后缀BUG
2. 增加动态数据源管理 增加测试连接功能
3. 修复根据角色获取菜单信息的问题
4. 数据字典增加redis缓存机制，提高系统性能
5. 优化添加页面操作体验
6. 更新代码生成器模版代码
7. 增加js自动生成随机数函数
8. 修复自增ID超长问题
9. 优化若干细节逻辑&修复若干BUG


#### Beta-v1.0.9 (2020.6.28)
1. 优化验证码刷新机制，防止暴力破解
2. 新增定时任务手动执行/自动执行操作
3. 修复系统日志异步保存异常BU
4. 更新配置文件application.xml
    >新增定时任务初始化状态配置参数jobInit定时任务初始化方式：
    >- 默认1:初始化全部停止状态；
    >- 2:初始化全部运行状态，并启动任务
    >- 3:保持原状态，并启动任务
5. 修改admin模块base64编码方式
6. 完善api模块sign签名算法
7. 开发文档新增前端调用API签名验证示例
8. 完善开发文档关于@signValid章节介绍
9. 完善@ApiToken注解校验token
10. 完善accessToken和refreshToken获取机制
>适配SQL版本1.0.12

### 捷码开源介绍

[关注牛逼的开源项目介绍](https://www.bilibili.com/video/BV1gK411J711/)

### 社区会员招募

https://www.gemframework.com/bbs/reging.html?channle=gem&inviter=2SEVLFH8


### 特别声明

近期发现一些不地道的人把项目源码拿去淘宝等电商平台恶意销售传播

为防止此行为猖獗请到此处获取SQL脚本：https://www.gemframework.com/bbs/thread/54.html

对于以上行为的个人或企业，官方绝不姑息，保留全部法律追究责任，严厉惩治！

请各位文明使用，取之有道，不要以身试法 ！

### SQL脚本下载

获取SQL脚本：https://www.gemframework.com/bbs/thread/54.html
或添加QQ群获取 [点击加入QQ群](https://qm.qq.com/cgi-bin/qm/qr?k=CvUg_DZ13GghgAOnawhJS7TVNg3QmGIg&jump_from=webapi)


### 演示站点
演示地址：[http://coral.gemframework.com:8088/admin](http://coral.gemframework.com:8088/admin)
演示帐号：admin
演示密码：123456


### 项目介绍   烤肉框架

> Coral是Gem家族成员之一，英文发音[ˈkɒrəl]谐音“烤肉”， 释义“珊瑚”。2020年首次与大家见面。她美如其名，不但外在美，内在更美...


Coral 企业快速开发框架，基于SpringBoot2.2x，MyBatis，Shiro等主流框架开发；前端页面采用LayUi开发。本系统技术栈选型专门面向后台开发人员快速上手而选，适合所有中小型企业或开发团队，开箱即用。http://www.gemframework.com

##### “一簇五彩斑斓的珊瑚” 

```
   ______                          ________
 .' ___  |                        |_   __  |
/ .'   \_|   .---.   _ .--..--.     | |_ \_|  _ .--.   ,--.    _ .--..--.    .---.
| |   ____  / /__\\ [ `.-. .-. |    |  _|    [ `/'`\] `'_\ :  [ `.-. .-. |  / /__\\
\ `.___]  | | \__.,  | | | | | |   _| |_      | |     // | |,  | | | | | |  | \__.,
 `._____.'   '.__.' [___||__||__] |_____|    [___]    \'-;__/ [___||__||__]  '.__.'

         GemFrame一款基于SpringBoot优秀的国产开源框架 http://www.gemframework.com


```


### 环境必备
   - Jdk8+
   - Mysql5.5+
   - Maven
   - Lombok（重要）
   
### 软件架构 (经典框架组合)
- 核心框架：Spring Boot 2.2.5.RELEASE
- 安全框架：Apache Shiro 1.4.2
- 模板引擎：Thymeleaf
- 前端：Layui 2.5.6, JQuery 3.3.1
- 持久层框架：MyBatis-Plus 3.3.1
- 关系型数据库: Mysql5.7
- 数据库连接池：Druid 1.1.10
- 缓存数据库: Redis 4.0.9
- 项目管理工具: Maven 3.3+
- 工具类：Hutool 4.5.8


### 技术选型
|技术栈|版本|前端技术|服务端技术|说明
|-|-|-|-|-|
|JDK|1.8x||✔|Java基础开发环境|
|Spring|5.0+||✔|IoC和AOP容器框架|
|SpringBoot|2.2.x||✔|简化配置的容器框架|
|Mybatis-Plus|3.3.x||✔|持久层框架|
|Shiro|1.6.0||✔|安全框架|
|MySQL|5.5+||✔|MySQL数据库|
|Oracle|10g+||✔|Oracle数据库|
|SQLServer|2000+||✔|MSSQL数据库|
|Maven|3.3+||✔|项目管理工具|
|Redis|3.2+||✔|NoSql中间件|
|Fastjson|1.2.58||✔|对象序列化工具|
|Swagger|2.6.1||✔|接口文档工具|
|Lombok|1.18||✔|提供注解简化编程|
|Captcha|1.6.2||✔|验证码工具|
|Druid|1.1.10||✔|数据库连接池|
|Thymeleaf|- - -||✔|视图模版技术|
|JQuery|3.2.1|✔||JavaScript代码库|
|Layui|v2.5.6|✔||Web弹层组件|
|Layer|2.5.6|✔||UI 框架|
|Layui.tree|- - -|✔||Web树形组件|
|Layui.xmSelect|- - -|✔||Web多选组件|
|Layui.treeTable|- - -|✔||树形表格组件|
|Layui.iconPicker|- - -|✔||图标选择器|
|Layui.cityPicker|- - -|✔||城市选择器|
|Layui.staps|- - -|✔||步骤操作组件|
|Layui.layDate|- - -|✔||时间选择器|
|Layui.QRcode|- - -|✔||二维码组件|
|Layui.Player|- - -|✔||播放器组件|

### 代码结构

- #### 模块结构
```
coral 
 |--coral-apirest 为前后端分离提供RESTful API
 |
 |--coral-admin 管理后台Web
 |
 |--coral-common 公共模块
 |
 |--coral-common-service 服务模块
 |
 |--coral-common-mapper 数据操作模块
 |
 |--coral-common-model 模型层

```
- #### 包结构
```
java
  |
  |--com.gemframework.common 公共包
  |--com.gemframework.common.annotation 公共自定义注解
  |--com.gemframework.common.config 公共配置
  |--com.gemframework.common.constant 公共常量
  |--com.gemframework.common.utils 公共工具包

  |--com.gemframework.constant 常量包
  |--com.gemframework.config 配置包
  |--com.gemframework.utils 工具包

  |--com.gemframework.controller 控制器包

  |--com.gemframework.mapper 映射器包 

  |--com.gemframework.model 模型类包
  |--com.gemframework.model.annotation 模型注解
  |--com.gemframework.model.common 公共模型
  |--com.gemframework.model.entity 实体对象
  |--com.gemframework.model.entity.po 持久对象
  |--com.gemframework.model.entity.vo 表现层对象
  |--com.gemframework.model.request API请求体对象
  |--com.gemframework.model.response API响应体对象

  |--com.gemframework.service 接口服务包
  |--com.gemframework.service.impl 接口实现包

```
- #### 静态资源包结构
```
resource
  |
  |--static 存放静态文件处 如css,js,img,fonts等
  |--templates 存放页面模版处 如html,jsp,vm等
  |--mapper 存放Mybatis SQL映射文件处

```
### 架构预览
![输入图片说明](https://images.gitee.com/uploads/images/2020/0619/113353_75331b26_2671379.png "jg.png")


### 系统截图

![login](https://images.gitee.com/uploads/images/2020/0530/222012_50876a63_2671379.jpeg "登录.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0530/222027_213b04bb_2671379.png "仪表盘.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0530/222047_6cec89a9_2671379.png "系统监控.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0530/222058_5ae3a869_2671379.png "权限管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0530/222107_3608aee9_2671379.png "角色管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0530/222118_92ea2c21_2671379.png "机构管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0530/222134_57c0ab56_2671379.png "字典管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0530/222151_c19feadb_2671379.png "阿里云.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0530/222201_36d033cc_2671379.png "oauth.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0530/222220_ba8965f7_2671379.png "Oauth2-gitee.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0530/222213_baefcf4b_2671379.png "授权登录.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0530/222237_2b45ff27_2671379.png "示例模块.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0530/222246_c6824317_2671379.png "大屏监控.png")

### 项目周边
- 项目文档：[限时免费](https://www.kancloud.cn/gemos/gem_coral_dev/1619676)
- 官方社区：[http://bbs.gemframework.com](http://www.gemframework.com/bbs)
- gitee仓库：https://gitee.com/gemteam/coral
- github仓库：https://github.com/gem-team/gem-croal
- 官方网站：http://www.gemframework.com
- 官方QQ群：72940788、446017307
- 关于更新：项目每周都会有更新,演示网站会在周五~周日,不定期暂停访问,带来不便尽情谅解!
- 如需关注项目最新动态，同时也是对项目最好的支持 技术讨论、二次开发等咨询、问题和建议，请移步到官方社区，我会在第一时间进行解答和回复！



### 随缘赞赏
<img src="https://images.gitee.com/uploads/images/2020/0324/225514_e980e3d0_2671379.png" width = "200" height = "200" div align=center />                                     
<img src="https://images.gitee.com/uploads/images/2020/0324/225522_cb4dd187_2671379.png" width = "200" height = "200" div align=center />                                     
 
- 赞赏请备注您的联系方式，非常感谢您的赞赏，我一直再努力!
- 支持我就帮忙在上方依次Watch、Star一波再离开，感激支持！
### 持续改进
##### 感谢您的支持，请允许借用您十分钟填一份满意度调查，使我们改进更好，您的声音对我很重要！
[满意度调查](https://www.wenjuan.com/s/6bAfMnE/?user=gitee&repeat=1)


